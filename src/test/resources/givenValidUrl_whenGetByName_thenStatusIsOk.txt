==RESPONSE==
{
  "_embedded": {
    "employeeList": [
      {
        "id": 1,
        "employeeNumber": "12345671",
        "firstName": "Alex1",
        "lastName": "Alexandru1",
        "email": "alex1@hba.com",
        "phoneNumber": "07719749801",
        "dateOfBirth": "06071981",
        "holidaysAvailable": 23,
        "jobRole": "ATM",
        "team": "RICHTEA_PACKING_HALL",
        "shift": "DAYS2",
        "active": true,
        "_links": {
          "self": {
            "href": "http://localhost/employee/1"
          },
          "all_employees": {
            "href": "http://localhost/employee{?name}",
            "templated": true
          }
        }
      },
      {
        "id": 2,
        "employeeNumber": "12345672",
        "firstName": "Alex2",
        "lastName": "Alexandru2",
        "email": "alex2@hba.com",
        "phoneNumber": "07719749802",
        "dateOfBirth": "06071982",
        "holidaysAvailable": 23,
        "jobRole": "ATM",
        "team": "RICHTEA_PACKING_HALL",
        "shift": "DAYS2",
        "active": true,
        "_links": {
          "self": {
            "href": "http://localhost/employee/2"
          },
          "all_employees": {
            "href": "http://localhost/employee{?name}",
            "templated": true
          }
        }
      }
    ]
  }
}