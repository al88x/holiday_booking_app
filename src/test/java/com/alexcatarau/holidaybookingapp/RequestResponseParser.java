package com.alexcatarau.holidaybookingapp;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;


@Component
public class RequestResponseParser {

    private final static String REQUEST = "==REQUEST==";
    private final static String RESPONSE = "==RESPONSE==";
    private static final String COULD_NOT_BE_PARSED = " could not be parsed";
    private String response;
    private String[] output;
    private String request;


    public RequestResponseParser() {
    }


    public String getRequest(final String filename) throws IOException {
        parseFile(filename);
        if (output != null) {
            request = output[0].replace(REQUEST, "");
        }
        return request;
    }


    public String getResponse(final String fileName) throws IOException {
        parseFile(fileName);
        if (output != null) {
            response = output[1];
        }
        return response;
    }


    private void parseFile(final String filename) throws IOException {
        String line = Files.lines(Paths.get(filename)).collect(Collectors.joining());
        if (line.contains(REQUEST) && line.contains(RESPONSE)) {
            this.output = line.split(RESPONSE);

        } else if (line.contains(REQUEST)) {
            this.request = line.replace(REQUEST, "");

        } else if (line.contains(RESPONSE)) {
            this.response = line.replace(RESPONSE, "");

        } else {
            throw new IOException(filename  + COULD_NOT_BE_PARSED);
        }
    }


}



