package com.alexcatarau.holidaybookingapp.rest;


import com.alexcatarau.holidaybookingapp.RequestResponseParser;
import com.alexcatarau.holidaybookingapp.persistence.model.Employee;
import com.alexcatarau.holidaybookingapp.persistence.model.builder.EmployeeBuilder;
import com.alexcatarau.holidaybookingapp.persistence.model.type.JobRole;
import com.alexcatarau.holidaybookingapp.persistence.model.type.Shift;
import com.alexcatarau.holidaybookingapp.persistence.model.type.Team;
import com.alexcatarau.holidaybookingapp.persistence.model.validator.ModelValidationException;
import com.alexcatarau.holidaybookingapp.rest.request.EmployeeRequest;
import com.alexcatarau.holidaybookingapp.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;
import java.util.stream.Stream;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest
public class EmployeeControllerTest {

    private static final String FILE_PATH = "src/test/resources/";
    private static final boolean STRICT_VERIFICATION = true;
    private static final String URL_PATH = "/employee/";
    private static final String WHEN_DELETE_THEN_STATUS_BAD_REQUEST = "Given Valid Request -> When Delete Employee -> Then Status 400 Bad Request";
    private static final String WHEN_DELETE_EMPLOYEE_THEN_STATUS_200_OK = "Given Valid Request -> When Delete Employee -> Then Status 200 Ok";
    private static final String WHEN_DELETE_EMPLOYEE_THEN_STATUS_IS_OK_TXT = "givenValidRequest_whenDeleteEmployee_thenStatusIsOk.txt";
    private static final String WHEN_PUT_EMPLOYEE_THEN_STATUS_400_BAD_REQUEST = "Given Valid Parameters -> When PUT employee -> Then Status 400 Bad Request";
    private static final String WHEN_PUT_EMPLOYEE_THEN_STATUS_BAD_REQUEST_TXT = "givenValidParameters_whenPutEmployee_thenStatusBadRequest.txt";
    private static final String LOCATION = "Location";
    private static final String HTTP_LOCALHOST_EMPLOYEE_1 = "http://localhost/employee/1";
    private static final String WHEN_PUT_EMPLOYEE_THEN_STATUS_201_IS_CREATED = "Given Valid Parameters -> When PUT  employee -> Then Status 201 is Created";
    private static final String WHEN_PUT_EMPLOYEE_THEN_STATUS_IS_CREATED_TXT = "givenValidParameters_whenPutEmployee_thenStatusIsCreated.txt";
    private static final String INVALID_PARAMETERS_WHEN_POST_NEW_EMPLOYEE_THEN_STATUS_400_BAD_REQUEST = "Given Invalid Parameters -> When POST new employee -> Then Status 400 Bad Request";
    private static final String INVALID_PARAMETERS_WHEN_POST_NEW_EMPLOYEE_THEN_STATUS_BAD_REQUEST_TXT = "givenInvalidParameters_whenPostNewEmployee_thenStatusBadRequest.txt";
    private static final String VALID_PARAMETERS_WHEN_POST_NEW_EMPLOYEE_THEN_STATUS_400_BAD_REQUEST = "Given Valid Parameters -> When POST new employee -> Then Status 400 Bad Request";
    private static final String VALID_PARAMETERS_WHEN_POST_NEW_EMPLOYEE_THEN_STATUS_BAD_REQUEST_TXT = "givenValidParameters_whenPostNewEmployee_thenStatusBadRequest.txt";
    private static final String VALID_PARAMETERS_WHEN_POST_NEW_EMPLOYEE_THEN_STATUS_201_CREATED = "Given Valid Parameters -> When POST new employee -> Then Status 201 Created";
    private static final String VALID_PARAMETERS_WHEN_POST_NEW_EMPLOYEE_THEN_STATUS_IS_CREATED_TXT = "givenValidParameters_whenPostNewEmployee_thenStatusIsCreated.txt";
    private static final String EMPLOYEE_NAME_ALEXANDRU = "/employee?name=Alexandru";
    private static final String ALEXANDRU = "Alexandru";
    private static final String WHEN_GET_BY_NAME_THEN_STATUS_404_NOT_FOUND_NO_EMPLOYEES = "Given valid Url -> When Get By Name -> Then Status 404 Not Found (no employees)";
    private static final String $_EMBEDDED_EMPLOYEE_LIST = "$._embedded.employeeList";
    private static final int RESPONSE_LIST_SIZE = 2;
    private static final String WHEN_GET_BY_NAME_THEN_STATUS_200_OK = "Given valid Url -> When Get By Name -> Then status 200 Ok";
    private static final String WHEN_GET_BY_NAME_THEN_STATUS_IS_OK_TXT = "givenValidUrl_whenGetByName_thenStatusIsOk.txt";
    private static final String WHEN_GET_BY_VALID_ID_THEN_STATUS_404_NOT_FOUND = "Given valid Url -> When Get By Valid Id -> Then status 404 Not Found ";
    private static final String WHEN_GET_BY_VALID_ID_THEN_STATUS_200_OK = "Given valid Url -> When Get By Valid Id -> Then Status 200 ok";
    private static final String WHEN_GET_BY_VALID_ID_THEN_STATUSIS_OK_TXT = "givenValidUrl_whenGetByValidId_thenStatusisOk.txt";
    private static final String WHEN_GET_ALL_THEN_STATUS_404_NOT_FOUND_NO_EMPLOYEES = "Given valid Url -> When Get All -> Then Status 404 Not Found (no employees)";
    private static final String WHEN_GET_ALL_THEN_STATUS_200_OK = "Given valid Url -> When Get All -> Then Status 200 ok";
    private static final String WHEN_GET_ALL_THEN_STATUS_IS_OK_TXT = "givenValidUrl_whenGetAll_thenStatusIsOk.txt";
    private Long employeeId;


    @MockBean
    private EmployeeService employeeService;
    private EmployeeBuilder employeeBuilder;
    private RequestResponseParser parser;
    private MockMvc mockMvc;
    private ObjectMapper mapper;


    @Test
    @DisplayName(WHEN_GET_ALL_THEN_STATUS_200_OK)
    @DirtiesContext
    void givenValidUrl_whenGetAll_thenStatusIsOk() throws Exception {
        when(employeeService.getAllEmployees()).thenReturn(provideListOfEmployees(2));

        String expectedJson = parser.getResponse(fromFile(WHEN_GET_ALL_THEN_STATUS_IS_OK_TXT));

        mockMvc.perform(get(URL_PATH))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8_VALUE))
                .andExpect(content().json(expectedJson, STRICT_VERIFICATION))
                .andExpect(MockMvcResultMatchers.jsonPath($_EMBEDDED_EMPLOYEE_LIST, hasSize(RESPONSE_LIST_SIZE)));
    }


    @Test
    @DisplayName(WHEN_GET_ALL_THEN_STATUS_404_NOT_FOUND_NO_EMPLOYEES)
    void givenValidUrl_whenGetAll_thenStatus404NotFound() throws Exception {
        when(employeeService.getAllEmployees()).thenReturn(Stream.empty());

        mockMvc.perform(get(URL_PATH))
                .andExpect(status().isNotFound());
    }


    @Test
    @DisplayName(WHEN_GET_BY_VALID_ID_THEN_STATUS_200_OK)
    void givenValidUrl_whenGetByValidId_thenStatusisOk() throws Exception {
        employeeId = 1L;
        Optional<Employee> employee = provideListOfEmployees(1).findFirst();

        when(employeeService.getEmployeeById(employeeId)).thenReturn(employee);

        String expectedJson = parser.getResponse(fromFile(WHEN_GET_BY_VALID_ID_THEN_STATUSIS_OK_TXT));

        mockMvc.perform(get(URL_PATH + employeeId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8_VALUE))
                .andExpect(content().json(expectedJson, STRICT_VERIFICATION));
    }


    @Test
    @DisplayName(WHEN_GET_BY_VALID_ID_THEN_STATUS_404_NOT_FOUND)
    void givenValidUrl_whenGetByValidId_thenStatusNotFound() throws Exception {
        employeeId = 123L;

        when(employeeService.getEmployeeById(employeeId)).thenReturn(Optional.empty());

        mockMvc.perform(get(URL_PATH + employeeId))
                .andExpect(status().isNotFound());
    }


    @Test
    @DisplayName(WHEN_GET_BY_NAME_THEN_STATUS_200_OK)
    void givenValidUrl_whenGetByName_thenStatusIsOk() throws Exception {
        when(employeeService.getEmployeesByName(ALEXANDRU)).thenReturn(provideListOfEmployees(2));

        String expectedJson = parser.getResponse(fromFile(WHEN_GET_BY_NAME_THEN_STATUS_IS_OK_TXT));

        mockMvc.perform(get(EMPLOYEE_NAME_ALEXANDRU))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8_VALUE))
                .andExpect(content().json(expectedJson, STRICT_VERIFICATION))
                .andExpect(MockMvcResultMatchers.jsonPath($_EMBEDDED_EMPLOYEE_LIST, hasSize(RESPONSE_LIST_SIZE)));
    }


    @Test
    @DisplayName(WHEN_GET_BY_NAME_THEN_STATUS_404_NOT_FOUND_NO_EMPLOYEES)
    void givenValidUrl_whenGetByName_thenStatus404() throws Exception {
        when(employeeService.getEmployeesByName(ALEXANDRU)).thenReturn(Stream.empty());

        mockMvc.perform(get(EMPLOYEE_NAME_ALEXANDRU))
                .andExpect(status().isNotFound());
    }


    @Test
    @DisplayName(VALID_PARAMETERS_WHEN_POST_NEW_EMPLOYEE_THEN_STATUS_201_CREATED)
    @DirtiesContext
    void givenValidParameters_whenPostNewEmployee_thenStatusIsCreated() throws Exception {

        String inputJson = parser.getRequest(fromFile(VALID_PARAMETERS_WHEN_POST_NEW_EMPLOYEE_THEN_STATUS_IS_CREATED_TXT));
        EmployeeRequest employeeBeforePost = mapper.readValue(inputJson, EmployeeRequest.class);
        Employee employeeAfterPost = mapper.readValue(parser
                .getResponse(fromFile(VALID_PARAMETERS_WHEN_POST_NEW_EMPLOYEE_THEN_STATUS_IS_CREATED_TXT)), Employee.class);

        when(employeeService.processAddEmployeeRequest(employeeBeforePost)).thenReturn(Optional.ofNullable(employeeAfterPost));

        mockMvc.perform(post(URL_PATH)
                .contentType((MediaType.APPLICATION_JSON_UTF8_VALUE))
                .content(inputJson))
                .andExpect(status().isCreated())
                .andExpect(header().string(LOCATION, HTTP_LOCALHOST_EMPLOYEE_1));
    }


    @Test
    @DisplayName(VALID_PARAMETERS_WHEN_POST_NEW_EMPLOYEE_THEN_STATUS_400_BAD_REQUEST)
    void givenValidParameters_whenPostNewEmployee_thenStatusBadRequest() throws Exception {
        String inputJson = parser.getRequest(fromFile(VALID_PARAMETERS_WHEN_POST_NEW_EMPLOYEE_THEN_STATUS_BAD_REQUEST_TXT));
        EmployeeRequest employeeBeforePost = mapper.readValue(inputJson, EmployeeRequest.class);

        when(employeeService.processAddEmployeeRequest(employeeBeforePost)).thenReturn(Optional.empty());

        mockMvc.perform(post(URL_PATH)
                .contentType((MediaType.APPLICATION_JSON_UTF8_VALUE))
                .content(inputJson))
                .andExpect(status().isBadRequest());
    }


    @Test
    @DisplayName(INVALID_PARAMETERS_WHEN_POST_NEW_EMPLOYEE_THEN_STATUS_400_BAD_REQUEST)
    void givenInvalidParameters_whenPostNewEmployee_thenStatusBadRequest() throws Exception {
        String inputJson = parser.getRequest(fromFile(INVALID_PARAMETERS_WHEN_POST_NEW_EMPLOYEE_THEN_STATUS_BAD_REQUEST_TXT));
        EmployeeRequest employeeBeforePost = mapper.readValue(inputJson, EmployeeRequest.class);

        when(employeeService.processAddEmployeeRequest(employeeBeforePost)).thenThrow(ModelValidationException.class);

        mockMvc.perform(post(URL_PATH)
                .contentType((MediaType.APPLICATION_JSON_UTF8_VALUE))
                .content(inputJson))
                .andExpect(status().isBadRequest());

    }

    @Test
    @DisplayName(WHEN_PUT_EMPLOYEE_THEN_STATUS_201_IS_CREATED)
    void givenValidParameters_whenPutEmployee_thenStatusIsCreated() throws Exception {
        String inputJson = parser.getRequest(fromFile(WHEN_PUT_EMPLOYEE_THEN_STATUS_IS_CREATED_TXT));
        EmployeeRequest employeeBeforeRequest = mapper.readValue(inputJson, EmployeeRequest.class);
        Employee employeeAfterRequest = mapper.readValue(parser
                .getResponse(fromFile(WHEN_PUT_EMPLOYEE_THEN_STATUS_IS_CREATED_TXT)), Employee.class);
        employeeId = 1L;

        when(employeeService.processUpdateEmployeeRequest(employeeId, employeeBeforeRequest))
                .thenReturn(Optional.ofNullable(employeeAfterRequest));

        mockMvc.perform(put(URL_PATH + employeeId)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(inputJson))
                .andExpect(status().isCreated())
                .andExpect(header().string(LOCATION, HTTP_LOCALHOST_EMPLOYEE_1));
    }


    @Test
    @DisplayName(WHEN_PUT_EMPLOYEE_THEN_STATUS_400_BAD_REQUEST)
    void givenValidParameters_whenPutEmployee_thenStatusBadRequest() throws Exception {
        String inputJson = parser.getRequest(fromFile(WHEN_PUT_EMPLOYEE_THEN_STATUS_BAD_REQUEST_TXT));
        EmployeeRequest employeeBeforeRequest = mapper.readValue(inputJson, EmployeeRequest.class);
        employeeId = 1L;

        when(employeeService.processUpdateEmployeeRequest(employeeId, employeeBeforeRequest))
                .thenReturn(Optional.empty());

        mockMvc.perform(put(URL_PATH + employeeId)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(inputJson))
                .andExpect(status().isBadRequest());
    }


    @Test
    @DisplayName(WHEN_DELETE_EMPLOYEE_THEN_STATUS_200_OK)
    void givenValidRequest_whenDeleteEmployee_thenStatusIsOk() throws Exception {
        employeeId = 1L;
        Employee employeeAfterRequest = mapper.readValue(parser
                .getResponse(fromFile(WHEN_DELETE_EMPLOYEE_THEN_STATUS_IS_OK_TXT)), Employee.class);

        when(employeeService.deleteEmployee(employeeId)).thenReturn(Optional.ofNullable(employeeAfterRequest));

        mockMvc.perform(delete(URL_PATH + employeeId))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName(WHEN_DELETE_THEN_STATUS_BAD_REQUEST)
    void givenValidRequest_whenDeleteEmployee_thenStatusBadRequest() throws Exception {
        employeeId = 1L;

        when(employeeService.deleteEmployee(employeeId)).thenReturn(Optional.empty());

        mockMvc.perform(delete(URL_PATH + employeeId))
                .andExpect(status().isBadRequest());
    }


    private String fromFile(String filename) {
        return FILE_PATH + filename;
    }


    private Stream<Employee> provideListOfEmployees(final int count) {
        return Stream
                .iterate(1, t -> t + 1)
                .limit(count)
                .map(integer -> {
                    Employee employee = employeeBuilder.newEmployee()
                            .addFirstName("Alex" + integer)
                            .addLastName("Alexandru" + integer)
                            .addEmail("alex" + integer + "@hba.com")
                            .addPhoneNumber("0771974980" + integer)
                            .addDateOfBirth("0607198" + integer)
                            .addJobRole(JobRole.ATM)
                            .addShift(Shift.DAYS2)
                            .addTeam(Team.RICHTEA_PACKING_HALL)
                            .addEmployeeNumber("1234567" + integer)
                            .addHolidaysAvailable(23)
                            .build().get();
                    employee.setId(Long.valueOf(integer));
                    return employee;
                });
    }


    @Autowired
    void setEmployeeBuilder(final EmployeeBuilder employeeBuilder) {
        this.employeeBuilder = employeeBuilder;
    }

    @Autowired
    void setParser(RequestResponseParser parser) {
        this.parser = parser;
    }

    @Autowired
    void setMockMvc(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @Autowired
    void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }
}
