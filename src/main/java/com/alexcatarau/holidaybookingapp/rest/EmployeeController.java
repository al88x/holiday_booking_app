package com.alexcatarau.holidaybookingapp.rest;


import com.alexcatarau.holidaybookingapp.persistence.model.Employee;
import com.alexcatarau.holidaybookingapp.rest.request.EmployeeRequest;
import com.alexcatarau.holidaybookingapp.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.web.bind.annotation.RequestMethod.*;


@RestController
@RequestMapping(path = "/employee")
public class EmployeeController {

    private static final String ID = "/{id}";
    public static final String ALL_EMPLOYEES = "all_employees";
    private EmployeeService employeeService;


    /*
     * POST EMPLOYEE
     */
    @RequestMapping(method = POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaTypes.HAL_JSON_UTF8_VALUE)
    public ResponseEntity addEmployee(@RequestBody final EmployeeRequest request) {
        Optional<Employee> optionalEmployee = employeeService.processAddEmployeeRequest(request);

        return optionalEmployee
                .map(employee -> linkTo(EmployeeController.class).slash(employee.getId()).toUri())
                .map(uri -> ResponseEntity.created(uri).build())
                .orElse(ResponseEntity.badRequest().build());
    }


    /*
     * UPDATE EMPLOYEE
     */
    @RequestMapping(method = PUT,
            value = ID,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaTypes.HAL_JSON_UTF8_VALUE)
    public ResponseEntity updateEmployee(@PathVariable final Long id, @RequestBody final EmployeeRequest request) {
        Optional<Employee> optionalEmployee = employeeService.processUpdateEmployeeRequest(id, request);

        return optionalEmployee
                .map(employee -> linkTo(EmployeeController.class).slash(employee.getId()).toUri())
                .map(uri -> ResponseEntity.created(uri).build())
                .orElse(ResponseEntity.badRequest().build());
    }


    /*
     *  DELETE EMPLOYEE
     */
    @RequestMapping(method = DELETE,
            value = ID)
    public ResponseEntity deleteEmployee(@PathVariable final Long id) {
        Optional<Employee> optionalEmployee = employeeService.deleteEmployee(id);

        return optionalEmployee
                .map(employee -> ResponseEntity.ok().build())
                .orElse(ResponseEntity.badRequest().build());
    }

    /*
     * GET Employee by name
     * GET ALL
     */


    @RequestMapping(method = GET,
            produces = MediaTypes.HAL_JSON_UTF8_VALUE)
    public ResponseEntity<Resources<Resource<Employee>>> getEmployees(@RequestParam(value = "name", required = false) final String name) {
        Stream<Employee> clients;

        if (name == null) {
            clients = employeeService.getAllEmployees();
        } else {
            clients = employeeService.getEmployeesByName(name);
        }

        List<Resource<Employee>> clientResources = clients
                .map(this::wrapEmployeeIntoResource)
                .collect(Collectors.toList());

        if (!clientResources.isEmpty()) {
            Resources<Resource<Employee>> resources = new Resources<>(clientResources);
            return ResponseEntity.ok(resources);
        }
        return ResponseEntity.notFound().build();
    }




    /*
     * GET EMPLOYEE by ID
     */
    @RequestMapping(value = ID,
            method = GET,
            produces = MediaTypes.HAL_JSON_VALUE)
    public ResponseEntity<Resource<Employee>> getEmployeeById(@PathVariable final Long id) {
        Optional<Employee> optionalClient = employeeService.getEmployeeById(id);

        return optionalClient
                .map(this::wrapEmployeeIntoResource)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }


    /*
     * Hateoas wrapper
     */
    private Resource<Employee> wrapEmployeeIntoResource(final Employee employee) {
        Link linkToSelf = linkTo(methodOn(EmployeeController.class).getEmployeeById(employee.getId())).withSelfRel();
        Link linkToAll = linkTo(methodOn(EmployeeController.class).getEmployees(null)).withRel(ALL_EMPLOYEES);

        return new Resource<Employee>(employee, linkToSelf, linkToAll);
    }


    @Autowired
    public void setEmployeeService(final EmployeeService employeeService) {
        this.employeeService = employeeService;
    }
}
