package com.alexcatarau.holidaybookingapp.rest.request;

import com.alexcatarau.holidaybookingapp.persistence.model.type.JobRole;
import com.alexcatarau.holidaybookingapp.persistence.model.type.Shift;
import com.alexcatarau.holidaybookingapp.persistence.model.type.Team;
import lombok.Data;


@Data
public class EmployeeRequest {

    private String employeeNumber;

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private String dateOfBirth;

    private int holidaysAvailable;

    private JobRole jobRole;

    private Team team;

    private Shift shift;

}
