package com.alexcatarau.holidaybookingapp.service;

import com.alexcatarau.holidaybookingapp.persistence.model.Employee;
import com.alexcatarau.holidaybookingapp.persistence.model.builder.EmployeeBuilder;
import com.alexcatarau.holidaybookingapp.persistence.model.log.Event;
import com.alexcatarau.holidaybookingapp.persistence.model.validator.ModelValidationException;
import com.alexcatarau.holidaybookingapp.repository.EmployeeRepository;
import com.alexcatarau.holidaybookingapp.rest.request.EmployeeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.Queue;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.alexcatarau.holidaybookingapp.persistence.model.type.EventType.*;


@Service
public class EmployeeService {

    private EmployeeBuilder employeeBuilder;
    private EmployeeRepository employeeRepository;
    private JmsTemplate jmsTemplate;
    private Queue jmsQueue;

    public Optional<Employee> processAddEmployeeRequest(final EmployeeRequest request) {
        Optional<Employee> optionalEmployee = employeeBuilder.createEmployee(request);
        optionalEmployee.ifPresent(employee -> {
            employeeRepository.save(employee);
            jmsTemplate.convertAndSend(jmsQueue, new Event(employee, EMPLOYEE_ADDED_EVENT));
        });
        return optionalEmployee;
    }


    public Optional<Employee> processUpdateEmployeeRequest(final Long id, final EmployeeRequest request) throws ModelValidationException {
        Optional<Employee> oldEmployeeOptional = employeeRepository.findById(id);

        return oldEmployeeOptional
                .map(oldEmployee -> employeeBuilder.updateEmployee(request, oldEmployee))
                .map(updatedEmployee -> {
                    employeeRepository.save(updatedEmployee.get());
                    jmsTemplate.convertAndSend(jmsQueue, new Event(updatedEmployee.get(), EMPLOYEE_UPDATED_EVENT));
                    return updatedEmployee.get();
                });
    }


    public Optional<Employee> deleteEmployee(final Long id) {
        Optional<Employee> optionalEmployee = employeeRepository.findById(id);

        optionalEmployee.ifPresent(employee -> {
            employee.setActive(false);
            employeeRepository.save(employee);
            jmsTemplate.convertAndSend(jmsQueue, new Event(employee, EMPLOYEE_DELETED_EVENT));
        });
        return optionalEmployee;
    }


    public Stream<Employee> getAllEmployees() {
        Iterable<Employee> allEmployees = employeeRepository.findAll();
        return StreamSupport.stream(allEmployees.spliterator(), false);
    }


    public Stream<Employee> getEmployeesByName(final String name) {
        return StreamSupport.stream(employeeRepository.findByLastName(name).spliterator(), false);
    }


    public Optional<Employee> getEmployeeById(final Long id) {
        return employeeRepository.findById(id);
    }


    @Autowired
    public void setEmployeeRepository(final EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }


    @Autowired
    public void setEmployeeBuilder(final EmployeeBuilder employeeBuilder) {
        this.employeeBuilder = employeeBuilder;
    }

    @Autowired
    public void setJmsTemplate(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Autowired
    public void setJmsQueue(Queue jmsQueue) {
        this.jmsQueue = jmsQueue;
    }
}
