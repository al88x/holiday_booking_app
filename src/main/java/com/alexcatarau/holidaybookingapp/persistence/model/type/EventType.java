package com.alexcatarau.holidaybookingapp.persistence.model.type;

public enum EventType {

    EMPLOYEE_ADDED_EVENT,
    EMPLOYEE_DELETED_EVENT,
    EMPLOYEE_UPDATED_EVENT
}
