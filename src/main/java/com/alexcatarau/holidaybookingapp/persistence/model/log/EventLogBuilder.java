package com.alexcatarau.holidaybookingapp.persistence.model.log;

import org.springframework.stereotype.Component;

@Component
public class EventLogBuilder {

    private EventLog eventLog;

    public EventLog createLogfromEvent(final Event event){
        return newEventLog()
                .addEventProperties(event)
                .build();
    }

    private EventLog build() {
        return eventLog;
    }

    private EventLogBuilder addEventProperties(Event event) {
        eventLog.setLocalTime(event.getLocalTime());
        eventLog.setPayload(event.getEmployee().toString());
        eventLog.setType(event.getEventType().name());
        eventLog.setEmployeeId(event.getEmployee().getId());

        return this;
    }

    private EventLogBuilder newEventLog() {
        eventLog = new EventLog();
        return this;
    }

}
