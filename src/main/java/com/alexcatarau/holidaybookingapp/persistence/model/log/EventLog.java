package com.alexcatarau.holidaybookingapp.persistence.model.log;


import lombok.AccessLevel;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "EVENTS")
@Setter(AccessLevel.PACKAGE)
public class EventLog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String localTime;

    @Column(length = 1000)
    private String payload;
    private String type;
    private Long employeeId;

}
