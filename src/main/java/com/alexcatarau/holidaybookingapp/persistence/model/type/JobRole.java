package com.alexcatarau.holidaybookingapp.persistence.model.type;

public enum JobRole {

    TM, ATM, MANAGER, ENGINEER

}
