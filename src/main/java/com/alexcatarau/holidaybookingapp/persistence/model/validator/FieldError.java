package com.alexcatarau.holidaybookingapp.persistence.model.validator;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class FieldError {

    private String field;
    private String errorMessage;


    public FieldError(final String field, final String errorMessage) {
        this.field = field;
        this.errorMessage = errorMessage;
    }




}
