package com.alexcatarau.holidaybookingapp.persistence.model.validator;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class GlobalExceptionHandler {


    @ExceptionHandler(ModelValidationException.class)
    public ResponseEntity treatGlobalValidationException(final ModelValidationException exception) {
        return ResponseEntity.badRequest().body(exception.getValidationErrors());
    }
}
