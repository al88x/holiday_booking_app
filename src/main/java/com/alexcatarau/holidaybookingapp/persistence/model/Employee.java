package com.alexcatarau.holidaybookingapp.persistence.model;

import com.alexcatarau.holidaybookingapp.annotation.UniqueEmployee;
import com.alexcatarau.holidaybookingapp.persistence.model.type.JobRole;
import com.alexcatarau.holidaybookingapp.persistence.model.type.Shift;
import com.alexcatarau.holidaybookingapp.persistence.model.type.Team;
import com.google.gson.Gson;
import lombok.Data;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;


@Entity
@Table(name = "EMPLOYEE")
@UniqueEmployee
@Data
public class Employee implements Identifiable<Long> {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Pattern(regexp = "^[0-9]{8}$", message = "Must have 8 digits")
    @Column(name = "employee_number")
    private String employeeNumber;

    @NotEmpty
    @Column(name = "first_name")
    private String firstName;

    @NotEmpty
    @Column(name = "last_name")
    private String lastName;

    @NotEmpty
    @Email
    @Column(name = "email")
    private String email;

    @NotEmpty
    @Column(name = "phoneNumber")
    private String phoneNumber;

    @NotEmpty
    @Column(name = "date_of_birth")
    private String dateOfBirth;

    @Column(name = "holidays_available")
    private int holidaysAvailable;

    @NotNull
    @Column(name = "job_role")
    private JobRole jobRole;

    @NotNull
    @Column(name = "team")
    private Team team;

    @NotNull
    private Shift shift;

    private boolean active;


    public Employee() {
    }


    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
