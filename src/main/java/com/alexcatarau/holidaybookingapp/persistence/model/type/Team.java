package com.alexcatarau.holidaybookingapp.persistence.model.type;

public enum Team {

    RICHTEA_PACKING_HALL, RICHTEA_PROCESS

}
