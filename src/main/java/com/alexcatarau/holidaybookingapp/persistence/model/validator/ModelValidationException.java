package com.alexcatarau.holidaybookingapp.persistence.model.validator;

import com.alexcatarau.holidaybookingapp.persistence.model.Employee;

import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class ModelValidationException extends ValidationException {


    private final List<FieldError> fieldErrorList;


    public ModelValidationException(final List<FieldError> fieldErrorList) {
        super();
        this.fieldErrorList = fieldErrorList;
    }


    public List<FieldError> getValidationErrors() {
        return fieldErrorList;
    }


    public static List<FieldError> extractValidationErrors(final Set<ConstraintViolation<Employee>> validationSet) {
        List<FieldError> errorList = new ArrayList<>();
        validationSet.stream()
                .forEach(constraintViolation ->
                        errorList.add(new FieldError(constraintViolation.getPropertyPath().toString(),
                                constraintViolation.getMessage())));
        return errorList;
    }

}
