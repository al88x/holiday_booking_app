package com.alexcatarau.holidaybookingapp.persistence.model.log;

import com.alexcatarau.holidaybookingapp.persistence.model.Employee;
import com.alexcatarau.holidaybookingapp.persistence.model.type.EventType;
import lombok.AccessLevel;
import lombok.Getter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
public class Event  {

    private Employee employee;
    private String localTime;
    private EventType eventType;


    public Event() {
    }

    public Event(final Employee employee, final EventType eventType) {
        this.employee = employee;
        this.eventType = eventType;
        localTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-YYYY HH:mm:ss"));
    }

}
