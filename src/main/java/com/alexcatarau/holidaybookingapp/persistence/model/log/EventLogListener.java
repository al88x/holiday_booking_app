package com.alexcatarau.holidaybookingapp.persistence.model.log;

import com.alexcatarau.holidaybookingapp.repository.EventLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import static com.alexcatarau.holidaybookingapp._config.JmsConfig.JMS_EMPLOYEE_QUEUE;


@Component
public class EventLogListener {

    private EventLogRepository eventLogRepository;
    private EventLogBuilder eventLogBuilder;


    @JmsListener(destination = JMS_EMPLOYEE_QUEUE)
    public void listener(final Event event) {
        eventLogRepository.save(eventLogBuilder.createLogfromEvent(event));
    }
    @Autowired
    public void setEventLogBuilder(EventLogBuilder eventLogBuilder) {
        this.eventLogBuilder = eventLogBuilder;
    }

    @Autowired
    public void setEventLogRepository(final EventLogRepository eventLogRepository) {
        this.eventLogRepository = eventLogRepository;
    }

}
