package com.alexcatarau.holidaybookingapp.persistence.model.builder;

import com.alexcatarau.holidaybookingapp.persistence.model.Employee;
import com.alexcatarau.holidaybookingapp.persistence.model.type.JobRole;
import com.alexcatarau.holidaybookingapp.persistence.model.type.Shift;
import com.alexcatarau.holidaybookingapp.persistence.model.type.Team;
import com.alexcatarau.holidaybookingapp.persistence.model.validator.FieldError;
import com.alexcatarau.holidaybookingapp.persistence.model.validator.ModelValidationException;
import com.alexcatarau.holidaybookingapp.rest.request.EmployeeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@Component
public class EmployeeBuilder {

    private Validator validator;

    @Autowired
    public void setValidator(final Validator validator) {
        this.validator = validator;
    }


    private Employee employee;

    public Optional<Employee> createEmployee(final EmployeeRequest request) {

        return newEmployee()
                .addRequestProperties(request)
                .build();
    }


    private EmployeeBuilder addRequestProperties(final EmployeeRequest request) {
        return this
                .addEmployeeNumber(request.getEmployeeNumber())
                .addFirstName(request.getFirstName())
                .addLastName(request.getLastName())
                .addEmail(request.getEmail())
                .addPhoneNumber(request.getPhoneNumber())
                .addDateOfBirth(request.getDateOfBirth())
                .addHolidaysAvailable(request.getHolidaysAvailable())
                .addJobRole(request.getJobRole())
                .addTeam(request.getTeam())
                .addShift(request.getShift());

    }


    public Optional<Employee> build() {
        Set<ConstraintViolation<Employee>> validationErrors = validator.validate(employee);
        if (!validationErrors.isEmpty()) {
            List<FieldError> fieldErrors = ModelValidationException.extractValidationErrors(validationErrors);
            throw new ModelValidationException(fieldErrors);
        }

        employee.setActive(true);
        return Optional.ofNullable(employee);
    }


    public EmployeeBuilder addTeam(final Team team) {
        this.employee.setTeam(team);
        return this;
    }


    public EmployeeBuilder addShift(final Shift shift) {
        this.employee.setShift(shift);
        return this;
    }


    public EmployeeBuilder addJobRole(final JobRole jobRole) {
        this.employee.setJobRole(jobRole);
        return this;
    }


    public EmployeeBuilder addHolidaysAvailable(final int holidaysAvailable) {
        this.employee.setHolidaysAvailable(holidaysAvailable);
        return this;

    }


    public EmployeeBuilder addDateOfBirth(final String dateOfBirth) {
        this.employee.setDateOfBirth(dateOfBirth);
        return this;
    }


    public EmployeeBuilder addPhoneNumber(final String phoneNumber) {
        this.employee.setPhoneNumber(phoneNumber);
        return this;
    }


    public EmployeeBuilder addEmail(final String email) {
        this.employee.setEmail(email);
        return this;
    }


    public EmployeeBuilder addLastName(final String lastName) {
        this.employee.setLastName(lastName);
        return this;
    }


    public EmployeeBuilder addFirstName(final String firstName) {
        this.employee.setFirstName(firstName);
        return this;
    }


    public EmployeeBuilder addEmployeeNumber(final String employeeNumber) {
        this.employee.setEmployeeNumber(employeeNumber);
        return this;
    }


    public EmployeeBuilder newEmployee() {
        this.employee = new Employee();
        return this;
    }


    public Optional<Employee> updateEmployee(final EmployeeRequest request, final Employee oldEmployee) {
        return createOldEmployee(oldEmployee)
                .addRequestProperties(request)
                .build();
    }


    private EmployeeBuilder createOldEmployee(final Employee oldEmployee) {
        this.employee = oldEmployee;
        return this;
    }
}
