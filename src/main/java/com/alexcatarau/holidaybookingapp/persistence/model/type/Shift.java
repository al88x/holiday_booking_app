package com.alexcatarau.holidaybookingapp.persistence.model.type;

public enum Shift {

    DAYS1, DAYS2, NIGHTS1, NIGHTS2

}
