package com.alexcatarau.holidaybookingapp.annotation;


import com.alexcatarau.holidaybookingapp.annotation.validator.UniqueEmployeeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


@Target({TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = {UniqueEmployeeValidator.class})
public @interface UniqueEmployee {

    String message() default "";


    Class<?>[] groups() default {};


    Class<? extends Payload>[] payload() default {};


}
