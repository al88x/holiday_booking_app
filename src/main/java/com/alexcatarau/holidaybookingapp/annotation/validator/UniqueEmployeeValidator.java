package com.alexcatarau.holidaybookingapp.annotation.validator;

import com.alexcatarau.holidaybookingapp.annotation.UniqueEmployee;
import com.alexcatarau.holidaybookingapp.persistence.model.Employee;
import com.alexcatarau.holidaybookingapp.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class UniqueEmployeeValidator implements ConstraintValidator<UniqueEmployee, Employee> {


    private EmployeeRepository employeeRepository;
    private final String ERROR_MESSAGE = "Employee with current EmployeeNumber already exists";
    private  final String FIELD = "employeeNumber";


    @Override
    public void initialize(final UniqueEmployee employee) {

    }

    @Override
    public boolean isValid(final Employee employee, final ConstraintValidatorContext context) {
        Employee repoEmployee = employeeRepository.findByEmployeeNumber(employee.getEmployeeNumber());

        if (repoEmployee != null && !(repoEmployee.getId().equals(employee.getId()))) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(ERROR_MESSAGE)
                    .addPropertyNode(FIELD).addConstraintViolation();
            return false;
        }
            return true;

    }


    @Autowired
    public void setEmployeeRepository(final EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }
}
