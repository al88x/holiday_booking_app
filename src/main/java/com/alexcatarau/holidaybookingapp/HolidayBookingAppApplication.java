package com.alexcatarau.holidaybookingapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HolidayBookingAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(HolidayBookingAppApplication.class, args);
	}

}
