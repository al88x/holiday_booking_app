package com.alexcatarau.holidaybookingapp.repository;

import com.alexcatarau.holidaybookingapp.persistence.model.log.EventLog;
import org.springframework.data.repository.CrudRepository;

public interface EventLogRepository extends CrudRepository<EventLog, Long> {
}
