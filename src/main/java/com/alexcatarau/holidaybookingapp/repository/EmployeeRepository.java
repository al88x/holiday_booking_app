package com.alexcatarau.holidaybookingapp.repository;

import com.alexcatarau.holidaybookingapp.persistence.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
    Employee findByEmployeeNumber(String employeeNumber);
    List<Employee> findByLastName(String name);
}
